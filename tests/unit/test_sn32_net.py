import pytest
import torch as th

from src.networks.sn_32 import Discriminator, Generator

# from torchinfo import summary


@pytest.mark.parametrize("in_c", [1, 3])
def test_disc(in_c):
    disc = Discriminator(df_dim=61, in_channel=in_c, d_spectral_norm=True)
    batch_size = 7
    input_size = (batch_size, in_c, 32, 32)
    sample_d = th.randn(input_size)
    assert disc.forward(sample_d).shape == (batch_size, 1)


@pytest.mark.parametrize("out_c", [1, 3])
def test_gen(out_c):
    gen = Generator(bottom_width=4, gf_dim=61, latent_dim=129, out_ch=out_c)
    batch_size = 7
    input_size = (batch_size, 129)
    assert gen.forward(th.randn(input_size)).shape == (7, out_c, 32, 32)
