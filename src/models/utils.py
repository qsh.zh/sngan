from torch import nn


def get_weights_init_fn(init_type="xavier_uniform"):
    def weights_init(m):
        classname = m.__class__.__name__
        if classname.find("Conv2d") != -1:
            if init_type == "normal":
                nn.init.normal_(m.weight.data, 0.0, 0.02)
            elif init_type == "orth":
                nn.init.orthogonal_(m.weight.data)
            elif init_type == "xavier_uniform":
                nn.init.xavier_uniform(m.weight.data, 1.0)
            else:
                raise NotImplementedError("{} unknown inital type".format(init_type))
        elif classname.find("BatchNorm2d") != -1:
            nn.init.normal_(m.weight.data, 1.0, 0.02)
            nn.init.constant_(m.bias.data, 0.0)

    return weights_init
