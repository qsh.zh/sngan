import torch as th
from hydra.utils import instantiate
from jamtorch.distributions.normal import StandardNormal
from omegaconf import DictConfig
from torch import nn

from .base_model import BaseModel
from .utils import get_weights_init_fn


# pylint: disable=too-many-ancestors, arguments-differ
class SNModel(BaseModel):
    def __init__(self, cfg: DictConfig):
        super().__init__(cfg)
        self.automatic_optimization = False

    def instantiate(self):
        self.generator = instantiate(self.cfg.generator)
        self.discriminator = instantiate(self.cfg.discriminator)
        self.z_dist = StandardNormal(self.z_shape)

    def on_pretrain_routine_start(self) -> None:
        super().on_pretrain_routine_start()
        # FIXME: fix the type
        weights_init_fn = get_weights_init_fn()
        self.generator.apply(weights_init_fn)
        self.discriminator.apply(weights_init_fn)

    def training_step(self, batch, batch_idx):
        del batch_idx
        real_imgs = self.data_transform_fn(batch)
        g_optimizer, d_optimizer = self.optimizers()
        # opt discriminator:
        d_optimizer.zero_grad()
        d_loss = self.opt_d(real_imgs)
        self.manual_backward(d_loss)
        d_optimizer.step()

        # opt generator
        if self.global_step % self.cfg.n_critic == 0:
            g_optimizer.zero_grad()
            g_loss = self.opt_g(real_imgs)
            self.manual_backward(g_loss)
            g_optimizer.step()

    def opt_d(self, real_imgs):
        real_validity = self.discriminator(real_imgs)
        fake_imgs = self.generator(self.z_dist.sample(real_imgs.shape[0])).detach()
        fake_validity = self.discriminator(fake_imgs)
        d_loss_real = th.mean(nn.ReLU(inplace=True)(1.0 - real_validity))
        d_loss_fake = th.mean(nn.ReLU(inplace=True)(1.0 + fake_validity))
        d_loss = d_loss_real + d_loss_fake
        self.log_dict(
            {
                "d_loss/real": d_loss_real,
                "d_loss/fake": d_loss_fake,
                "d_loss/loss": d_loss,
            },
            on_step=True,
            prog_bar=True,
        )
        return d_loss

    def opt_g(self, real_imgs):
        batch_size = real_imgs.shape[0] * 2
        del real_imgs
        fake_imgs = self.generator(self.z_dist.sample(batch_size))
        fake_validity = self.discriminator(fake_imgs)
        g_loss = -th.mean(fake_validity)
        self.log_dict(
            {"g_loss/loss": g_loss},
            on_step=True,
            prog_bar=True,
        )
        return g_loss

    def configure_optimizers(self):
        # in cifar we betas=(0.0, 0.9)
        # beta_1 ignore moments, use current gradient use moments
        # beta_2 large accumulate of the gradient magnitude
        opt_g = th.optim.Adam(
            self.generator.parameters(),
            lr=self.cfg.g_lr,
            betas=(self.cfg.beta1, self.cfg.beta2),
        )
        opt_d = th.optim.Adam(
            self.discriminator.parameters(),
            lr=self.cfg.d_lr,
            betas=(self.cfg.beta1, self.cfg.beta2),
        )
        return opt_g, opt_d

    def forward(self, z_vars=None, num_sample=None):
        if z_vars is not None:
            return self.generator(z_vars)
        else:
            return self.sample_n(num_sample)

    def sample_n(self, num_sample):
        return self.generator(self.z_dist.sample(num_sample))

    def logp(self, x):
        return self.discriminator(x)
